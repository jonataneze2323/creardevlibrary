package com.creardev.mobile.creardevlibrary.assets.enums;

/**
 * @version 0.0.1
 * @author Luis Hernandez
 */
public enum ErrorException {
    DuplicateDocumentId,DuplicateEmail,PasswordMismatch
}
