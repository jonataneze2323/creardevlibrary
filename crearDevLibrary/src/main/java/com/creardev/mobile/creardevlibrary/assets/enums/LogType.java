package com.creardev.mobile.creardevlibrary.assets.enums;

/**
 * Log type definitions
 *
 * @author Luis Hernández
 * @version 1.0
 */
public enum LogType {
    INFO, DEBUG, WARNING, ERROR
}
