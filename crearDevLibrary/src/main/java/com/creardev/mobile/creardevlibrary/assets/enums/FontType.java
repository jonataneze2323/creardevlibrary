package com.creardev.mobile.creardevlibrary.assets.enums;

/**
 * Custom fonts enumerable
 *
 * @author Ezequiel Alvarez
 * @version 1.0
 */
public enum FontType {

    ROBOTO_BLACK("fonts/roboto/Roboto-Black.ttf"),
    ROBOTO_BLACK_ITALIC("fonts/roboto/Roboto-BlackItalic.ttf"),
    ROBOTO_BOLD("fonts/roboto/Roboto-Bold.ttf"),
    ROBOTO_BOLD_ITALIC("fonts/roboto/Roboto-BoldItalic.ttf"),
    ROBOTO_ITALIC("fonts/roboto/Roboto-Italic.ttf"),
    ROBOTO_LIGHT("fonts/roboto/Roboto-Light.ttf"),
    ROBOTO_LIGHT_ITALIC("fonts/roboto/Roboto-LightItalic.ttf"),
    ROBOTO_MEDIUM("fonts/roboto/Roboto-Medium.ttf"),
    ROBOTO_MEDIUM_ITALIC("fonts/roboto/Roboto-MediumItalic.ttf"),
    ROBOTO_REGULAR("fonts/roboto/Roboto-Regular.ttf"),
    ROBOTO_THIN("fonts/roboto/Roboto-Thin.ttf"),
    ROBOTO_THIN_ITALIC("fonts/roboto/Roboto-ThinItalic.ttf"),
    CONFORTAA_BOLD("fonts/comfortaa/Comfortaa-Bold.ttf"),
    CONFORTAA_LIGHT("fonts/comfortaa/Comfortaa-Light.ttf"),
    CONFORTAA_REGULAR("fonts/comfortaa/Comfortaa-Regular.ttf"),
    POSTER("fonts/vtks/poster.ttf");

    private String path;

    FontType(String path) {
        this.path = path;
    }

    public String getAssetPath() {
        return path;
    }

    public static FontType getDefault() {
        return ROBOTO_REGULAR;
    }
}
